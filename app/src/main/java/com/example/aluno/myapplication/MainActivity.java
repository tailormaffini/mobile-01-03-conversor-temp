package com.example.aluno.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Spinner spinDe, spinPara;
    TextView textDe, textPara;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinDe = findViewById(R.id.spinnerDe);
        spinPara = findViewById(R.id.spinnerPara);
        textDe = findViewById(R.id.etDe);
        textPara = findViewById(R.id.etPara);
    }

    public void clickCalcular(View view){
        try {
            textPara.setText(textDe.getText().toString());
            if(spinDe.getSelectedItem().toString().equals("Celcius")){
                if(spinPara.getSelectedItem().toString().equals("Fahrenheit")){
                    textPara.setText(CtoF(textDe.getText().toString()));
                }
                if(spinPara.getSelectedItem().toString().equals("Kelvin")){
                    textPara.setText(CtoK(textDe.getText().toString()));
                }
            } else if(spinDe.getSelectedItem().toString().equals("Fahrenheit")){
                if(spinPara.getSelectedItem().toString().equals("Celcius")){
                    textPara.setText(FtoC(textDe.getText().toString()));
                }
                if(spinPara.getSelectedItem().toString().equals("Kelvin")){
                    textPara.setText(FtoK(textDe.getText().toString()));
                }
            } else if(spinDe.getSelectedItem().toString().equals("Kelvin")){
                if(spinPara.getSelectedItem().toString().equals("Fahrenheit")){
                    textPara.setText(KtoF(textDe.getText().toString()));
                }
                if(spinPara.getSelectedItem().toString().equals("Celcius")){
                    textPara.setText(KtoC(textDe.getText().toString()));
                }
            }
        } catch (Exception e){
        }

    }

    private String CtoF(String celcius){
        double fahrenheit = (1.8 * Double.valueOf(celcius)) + 32;
        return String.valueOf(fahrenheit);
    }
    private String CtoK(String celcius){
        double kelvin = Double.valueOf(celcius) + 273;
        return String.valueOf(kelvin);
    }
    private String FtoC(String fahrenheit){
        double celcius = (Double.valueOf(fahrenheit) - 32) / 1.8;
        return String.valueOf(celcius);
    }
    private String FtoK(String fahrenheit){
        double kelvin = Double.valueOf(FtoC(fahrenheit)) + 273 ;
        return FtoC(fahrenheit);
    }
    private String KtoF(String kelvin){
        double fahrenheit = (1.8 * Double.valueOf(KtoC(kelvin))) + 32;
        return String.valueOf(fahrenheit);
    }
    private String KtoC(String kelvin){
        double celcius = Double.valueOf(kelvin) - 273;
        return String.valueOf(celcius);
    }
}
